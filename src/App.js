
import Container from 'react-bootstrap/esm/Container';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import './App.css';


import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Admin from './pages/Admin';
import Products from './pages/Products';
import ProductView from './pages/ProductView';


import { UserProvider } from './UserContext';
import { useState } from 'react';

function App() {

  const [user,setUser] =useState({
    id: localStorage.getItem('id'),
   
  })

  const unsetUser = () =>{
    localStorage.clear();
  }


  return (
  <UserProvider value = {{user, setUser, unsetUser}}>

   <Router>
    <Container fluid>
      <AppNavbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Admin" element={<Admin />} />
        <Route path="/Products" element={<Products />} />
        <Route path="/Products/:productId" element={<ProductView />} />
        <Route path="/Register" element={<Register />} />
        <Route path="/Login" element={<Login />} />
        <Route path="/Logout" element={<Logout />} />
      </Routes>

    </Container>
   </Router>
   </UserProvider>
  );
}

export default App;
