import { Fragment } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from 'react-router-dom';
import { useState } from 'react';



export default function AppNavbar() {

  const [userId, setUserId] = useState(localStorage.getItem("userId"));

  return (
    <>
      <Navbar bg="dark" variant="dark">
      <Navbar.Brand href="#home">EmShop</Navbar.Brand>
        <Container>
          
          <Nav className="m-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/Products">Products</Nav.Link>
            <Nav.Link as={NavLink} to="/Admin">AdminDashBoard</Nav.Link>
            
            {(userId === null)?
            
              <Fragment>
              
              <Nav.Link as={NavLink} to="/Register">Register</Nav.Link>
              <Nav.Link as={NavLink} to="/Login">Login</Nav.Link>
              
              
              </Fragment>
               
             :

          
            <Nav.Link as={NavLink} to="/Logout">Logout</Nav.Link>
            
            }
          </Nav>
        </Container>
      </Navbar>
    
     
    </>
  );
}

