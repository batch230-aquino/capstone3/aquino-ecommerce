import {Card, Button} from 'react-bootstrap';
import  PropTypes  from 'prop-types';
import  { Link } from 'react-router-dom';


export default ProductsCard;


function ProductsCard({productProp}) {

  const {_id, productName, description, price, stocks} = productProp;

  return (
    <Card>
    
    <Card.Body>
        <Card.Title>
        {productName}
        </Card.Title>

        <Card.Subtitle>
            Description:
        </Card.Subtitle>

        <Card.Text>
        {description}
        </Card.Text>

        <Card.Text>
        Price:
        </Card.Text>

        <Card.Text>
        {price}
        </Card.Text>

        <Card.Text>
        Stocks:
        </Card.Text>

        <Card.Text>
        {stocks}
        </Card.Text>

        <Button as={Link} to ={`/products/${_id}`}>Add to Cart</Button>
      

    </Card.Body>
    </Card>
    
)
}