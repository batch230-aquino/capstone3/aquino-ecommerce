import { Fragment } from "react";
import Banner from "../components/Banner";

const data = {
    title: "Em Computer PC parts",
    content: "PC parts for everyone, anyone and everywhere!!",
    destination: "/",
    label: "Buy now!"
}


export default function Home(){

    return(
        <Fragment>
            <Banner data={data} />
        </Fragment>
    )
}