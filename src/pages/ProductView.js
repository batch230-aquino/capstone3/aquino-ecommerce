import { Row, Col, Container, Button, Card } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate, Link }  from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){

    const navigate = useNavigate ();
    const {productId} = useParams();
    const { user } = useContext(UserContext);

    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    useEffect(() => {
        console.log(productId);

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProductName(data.productName);
            setDescription(data.description);
            setPrice(data.price)
        })
    }, [productId])

    const addtocart = (productId) => {

        fetch(`${process.env.REACT_APP_API_URL}/orders/addtocart`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId : productId
            })

        })
        .then(res => res.json())
        .then(data => {

        console.log("add to cart data");
        console.log(data);

           if(data){

               Swal.fire({
                   title: "Successfully add to cart",
                   icon: "success",
                   text: "You have successfully add to cart product"
               })
               navigate("/Products")
           }
           else{
               Swal.fire({
                   title: "Sometimes went wrong",
                   icon: "error",
                   text: "Please try again."
               })
           }
            
        })
    }
    return(
        <Container className="mt-5">
           <Row>
            <Col lg={{span:6, offset: 3}}>
                <card>
                <Card.Body className="text-center">
							<Card.Title>{productName}</Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>

                            <Card.Subtitle>Stocks:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							
							{
								(user.id !== null)
								?
									<Button variant="primary"  size="lg" onClick={() => addtocart(productId)}>Add to Cart</Button>
								:
									<Button as={Link} to="/Login" variant="success"  size="lg">Login to Purchase</Button>
							}
						</Card.Body>		
                </card>

            </Col>
           </Row> 
        </Container>
    )
}


