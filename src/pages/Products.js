import { useContext, useEffect,useState } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import ProductsCard from "../components/ProductsCard";



export default function Products(){

    
    const { user } = useContext(UserContext);


    console.log(user);

    const [ products, setProducts ] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProducts(data.map(product  =>{
                return(
                    <ProductsCard key={product._id} productProps={product} />
                )
            }))
        })
    }, []);

    
    return(
        
        (user.isAdmin)?
        
        <Navigate to = "/Admin" />
        :
        <>
            <h1>Products</h1>
            {products}
        </>
    )
}