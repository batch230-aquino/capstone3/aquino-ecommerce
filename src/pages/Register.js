
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { useState, useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import Swal from "sweetalert2";
import { FaEye, FaEyeSlash } from 'react-icons/fa';


export default function Register(){

    const { user } = useContext(UserContext);
    const navigate = useNavigate();
    
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [showPassword, setShowPassword] = useState(false);
    const passwordVisibility = () => setShowPassword(!showPassword);
    const [isEnable, setIsEnable] = useState(false);

    //checker
    // console.log(firstName);
    // console.log(lastName);
    // console.log(email);
    // console.log(password1);
    // console.log(password2);
    // console.log(mobileNo);



    function signUpUser(e){

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/emailChecker`,{
            method: 'POST',
            headers:{'Content-Type' : 'application/json'},
            body: JSON.stringify({email: email})
        })
        .then(res => res.json())
        .then(data =>{
            
            console.log(data);

            if(data){
                
                Swal.fire({
                    title:"Duplicate Email Found",
                    icon: "error",
                    text: "please use a different email"
                })
                navigate ("/Register")
            }
            else{
                fetch(`${process.env.REACT_APP_API_URL}/users/Registration`,{
                    method: 'POST',
                    headers:{'Content-Type' : 'application/json'},
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email:email,
                        password: password2,
                        mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data =>{
                    console.log(data)

                    Swal.fire({
                        title: "Registration Success",
                        icon: "success",
                        text: "Welcome to my EMShop!"

                    })
                    navigate ("/Login")
                })
            }

        })
    }

    useEffect(() =>{

        if ((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNo !== '')
        && (password1 === password2)){
            
            setIsEnable(true);
        }
        else{
            setIsEnable(false);
        }
    }, [firstName,lastName,email,password1,password2,mobileNo])





    return(

        (user.id !== null)?

        <navigate to ="/" />
        :

    <div style={{ display: 'block', width: 700, padding: 30 }}>

            <h4>Register Page</h4>

        <Form onSubmit={(e => signUpUser(e))}>
            <Form.Group>
                    <Form.Label>Enter your First Name:</Form.Label>
                    <Form.Control 
                    type="text" 
                    placeholder="Enter your first name"
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                     />
            </Form.Group>

            <Form.Group>
                    <Form.Label>Enter your Last Name:</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter your last name" 
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                        />
            </Form.Group>

            <Form.Group>
                    <Form.Label>Enter your email address:</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter your your email address"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required 
                        />
            </Form.Group>
            

                
            
            <Form.Group>
                <Form.Label>Enter your password:</Form.Label>
                <div className='password'>
                <Form.Control 
                type={showPassword? 'text' : 'password'} 
                placeholder="password must be contain 7 characters/numbers"
                value={password1}
                onChange={e => setPassword1(e.target.value)}
                required 
                 />
                {password1 ? 
                        <FaEyeSlash onClick={passwordVisibility} id="eye" />  
                        : 
                        <FaEye onClick={passwordVisibility} id="eye"  />
                        }
                </div>
              
            </Form.Group>

            <Form.Group>
                <Form.Label>Enter your verified password:</Form.Label>
                <div className='password'>
                <Form.Control 
                type={showPassword? 'text' : 'password'} 
                placeholder="Enter your password again"
                value={password2}
                onChange={e => setPassword2(e.target.value)}
                required 
                 />
                  {password2 ? 
                        <FaEyeSlash onClick={passwordVisibility} id="eye" />  
                        : 
                        <FaEye onClick={passwordVisibility} id="eye"  />
                        }
                 </div>
                </Form.Group>
                

            <Form.Group>
                <Form.Label>Enter your Mobile Number:</Form.Label>
                <Form.Control 
                type="number" 
                placeholder="Enter your Mobile Number"
                value={mobileNo}
                onChange={e => setMobileNo(e.target.value)}
                required 
                 />
            </Form.Group>
                {isEnable?
                    <Button variant="success" type="submit" className='mt-3' >
                Click here to submit form
                </Button>

                :

                <Button variant="primary" type="submit" className='mt-3' disabled>
                Click here to submit form
                </Button>
                }
            

        </Form>
    </div>
        )
    }